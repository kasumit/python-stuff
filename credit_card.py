
def is_valid_card(card=None):
    sum_of_even_numbers = sum([int(card[i]) for i in range(len(card)) if i % 2 == 1 ])
    sum_of_odd_numbers_by_order = sum([sum(map(lambda n: int(n), list(str(int(card[i]) * 2)))) for i in range(len(card)) if i % 2 == 0])
    return (sum_of_even_numbers + sum_of_odd_numbers_by_order) % 10 == 0


if __name__ == '__main__':
    result = is_valid_card(card="")
    print(result)
